import 'package:flutter/widgets.dart';
class Product{
  late String _productName;
  late double _productPrice;
  late String _image;
  late String _description;
  Product (String name, double price, String im, String descrip){
    _productName = name;
    _productPrice = price;
    _image = "assets/images/$im.jpg";
    _description = descrip;
  }
  String getName(){return _productName;}
  double getPrice(){return _productPrice;}
  String getImage(){return _image;}
  String getDescription(){return _description;}

  void setName(String name){_productName = name;}
  void setPrice(double price){_productPrice = price;}


}
