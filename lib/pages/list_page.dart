import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pr2/product_model/product_model.dart';
import 'package:pr2/pages/product_details_page.dart';

class ListPage extends StatelessWidget{
  static final List <Product> initial = [
    Product("Router TP-Link AC1200",99.99, "RouterTPlink","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Router Tenda AC6", 26.09, "routerTenda", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Switch TP-Link TL-SG105", 3.90, "Switch", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("EPSON Workforce ES-50", 108.83, "EPSON", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("ScanMarker Air", 119.00, "ScanMarker", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Apple iPad Air 16GB", 269.89, "ipad", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Echo Show 5", 80.99, "echoShow 5", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Echo Dot", 44.99, "echoDot", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Google Nest Hub", 94.06, "googleNest Hub", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
    Product("Oculus Quest", 549.00, "oculusQuest", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet magna ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit..."),
  ];
  @override
  Widget build(BuildContext context){
    final list = initial.map((producto) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: "${producto.getName()}",
              child: Material(
                child: ListTile(
                  tileColor: Colors.grey.shade100,
                  onTap:(){
                    var route = MaterialPageRoute(
                      builder: (context) => ProductDetailsPage(data: producto, heroTag: "${producto.getName()}", heroImg: "${producto.getImage()}"),
                    );
                    Navigator.of(context).push(route);
                  },
                  leading: Container(
                    height: 70,
                    width: 70,
                    decoration: new BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          "${producto.getImage()}",
                        ),
                      ),
                    ),
                  ),
                  title: Text("${producto.getName()}",
                  ),
                  subtitle: Text("${producto.getPrice()}"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined),
                ),
              ),
            ),
            Divider(
              height: 80,
              thickness: 0.2,
              color: Colors.grey,
            ),
          ],
      );
    } ).toList();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children:[
            Text(
              "Productos",
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Colors.white, fontSize: 25.0,
                fontFamily: 'Roboto',
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.grey.shade100,
      body: SafeArea(
        child: ListView.builder(
          padding: EdgeInsets.all(15),
          itemCount: list.length,
          itemBuilder: (context, i) => list[i],
        ),
      ),
    );
  }
}