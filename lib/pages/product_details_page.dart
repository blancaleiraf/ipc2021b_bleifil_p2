import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pr2/product_model/product_model.dart';
//import 'package:tuple/tuple.dart';

class ProductDetailsPage extends StatelessWidget {

  late final String heroTag;
  late final String heroImg;
  late final Product data;

  ProductDetailsPage({required this.data, required this.heroTag, required this.heroImg});

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text(
          "Detalles del producto",
          textAlign: TextAlign.start,
          style: TextStyle(
            color: Colors.white, fontSize: 25.0,
            fontFamily: 'Roboto',
          ),
        ),
      ),
      backgroundColor: Colors.grey.shade300,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _mainImage(),
              ],
            ),
            Spacer(),
            _productInfo(),
          ],
        ),
      ),
    );
  }
  Widget _mainImage(){
    return Hero(
      tag: heroTag,
      child: Container(
        margin: EdgeInsets.all(20),
        height: 350,
        width: 350,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          image: DecorationImage(
            image: AssetImage(heroImg),
            scale: 2,
          ),
        ),
      ),
    );
  }
  Widget _Txt(String str, double size, double h){
    return Text(
      str,
      style: TextStyle(
        height: h,
        fontSize: size,
      ),
    );
  }
  Widget _productInfo (){
    return Container(
      padding: EdgeInsets.all(40),
      height: 390,
      width: 450,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.only(
          topStart: Radius.circular(30),
          topEnd: Radius.circular(30),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _Txt(data.getName(), 25, 1),
          _Txt("€ ${data.getPrice()}", 25, 2),
          _Txt(data.getDescription(), 20, 1),
        ],
      ),
    );
  }
}