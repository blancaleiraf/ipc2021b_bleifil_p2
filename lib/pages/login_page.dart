import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pr2/pages/list_page.dart';
class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _Cabecera(),
            Spacer(),
            _Center(),
            Spacer(),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: (){
                    var route = MaterialPageRoute(
                      builder: (context) => ListPage(),
                    );
                    Navigator.of(context).push(route);
                  },
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "LOGIN",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontSize: 25,
                          fontFamily: 'Roboto',
                        ),
                      ),
                      width: 225,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.deepPurple,
                        borderRadius: BorderRadiusDirectional.only(
                          topStart: Radius.circular(40),
                          bottomStart:Radius.circular(40),
                        ),
                        boxShadow:[
                          BoxShadow(
                            color: Colors.black26,
                            spreadRadius: 10,
                            blurRadius: 20,
                          ),
                        ],
                      )
                  ),
                ),
              ],
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

Widget _Cabecera (){
  return Row(
    children: [
      Padding(padding: EdgeInsets.all(15.0)),
      Text("Login",
        style: TextStyle(
          color: Colors.black,
          fontSize: 47.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'Roboto',
        ),
      ),
      Spacer(),
      Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.deepPurple,
          shape: BoxShape.circle,
          boxShadow:[
            BoxShadow(
              color: Colors.black26,
              spreadRadius: 12,
              blurRadius: 15,
            ),
          ],
        ),
        margin: const EdgeInsets.only(right: 10.0),
      ),
      Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.deepPurple,
          shape: BoxShape.circle,
          boxShadow:[
            BoxShadow(
              color: Colors.black26,
              spreadRadius: 10,
              blurRadius: 20,
            ),
          ],
        ),
        margin: const EdgeInsets.only(right: 15.0),
      ),
    ],
  );
}

Widget _Center () {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: [
      Container(
        padding: EdgeInsets.all(30),
        width: 340,
        height: 230,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              spreadRadius: 10,
              blurRadius: 20,
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Spacer(),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(
                  IconData(62332, fontFamily: 'MaterialIcons'),
                  color: Colors.deepPurple,
                ),
                labelText: 'Username',
              ),
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
            Spacer(),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(
                  IconData(58625, fontFamily: 'MaterialIcons'),
                  color: Colors.deepPurple,
                ),
                labelText: 'Password',
              ),
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    ],
  );
}